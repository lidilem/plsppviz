var pauseThreshold = { 'min':0.180, 'max':2 }
document.getElementById('pauseDurSetMin').value = pauseThreshold.min
document.getElementById('pauseDurSetMax').value = pauseThreshold.max
var minNbTokens = 0
document.getElementById('minNbTokens').value = minNbTokens

var clauseTag = ["S","SBAR","SBARQ","SINV","SQ"]
var phraseTag = ["ADJP","ADVP","CONJP","FRAG","INTJ","LST","NAC","NP","NX","PP","PRN","PRT","QP","RRC","UCP","VP","WHADJP","WHAVP","WHNP","WHPP","X"]
var conjTag = ["CC","IN"]

// Get specific request in URL if any
const urlParams = new URLSearchParams(window.location.search);
var threshold = urlParams.get('threshold')
const req = urlParams.get('req')

if (threshold) {
    threshold = threshold.split('-')
    console.log(threshold)
    if (threshold.length == 2) {
        var thremin = Number(threshold[0])
        var thremax = Number(threshold[1])
        document.getElementById('pauseDurSetMin').value = thremin
        document.getElementById('pauseDurSetMax').value = thremax
        console.log(thremin,thremax)
        if (thremin>0 && thremax>thremin) {
            pauseThreshold.min = thremin
            pauseThreshold.max = thremax
            if (document.getElementById('speakers').children.length>1) updateSettings() // don't update if speakers not loaded yet.
        }
    }
}

if (req) {
    console.log("Request:",req);
    getSpeakerSegments(req);
}

function updateSettings() {
    pauseThreshold.min = Number(document.getElementById('pauseDurSetMin').value);
    pauseThreshold.max = Number(document.getElementById('pauseDurSetMax').value);
    minNbTokens = Number(document.getElementById('minNbTokens').value);
    console.log("Settings updated!")
    console.log(pauseThreshold, minNbTokens)
    printText()
}

getSpeakers();


function goToView(view) {
    if (view=="global") {
        window.location = '/'+dataset;
    } else if (view=="stress") {
        window.location = '/segments/'+dataset;
    } else if (view=="pauses") {
        if (["CLES"].includes(dataset)) {
            window.location = '/pausesviz/'+dataset;
        } else {
            window.alert("Pauses analysis has not been done yet on this dataset.")
        }
    }
}


// Get list of speakers
var locuteurs = {};
async function getSpeakers() {
   
    var colis = {
        "dataset": dataset
    }
    console.log('Envoi',colis)
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    }
    // ENVOI
    const response = await fetch('/_getSpeakers/', options);
    const data = await response.json();
    console.log(data);
    locuteurs = data;

    document.getElementById('speakers').innerHTML = "<option value='none' selected>Select</option>";
    for (loc in locuteurs) {
        document.getElementById('speakers').innerHTML += `<option value="${locuteurs[loc]['speakerID']}">${locuteurs[loc]['speakerID']}</option>`
    }

    makeSegmentsStats()
}

var segments = [];
var segmentsName = [];

// Get segments for target speaker
async function getSpeakerSegments(loc) {
   
    var colis = {
        "speaker": loc,
        "dataset": dataset,
        "minNbTokens": minNbTokens
    }
    console.log(colis)
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    }
    // ENVOI
    const response = await fetch('/_getSpeakerSegments/', options);
    const data = await response.json();
    console.log(data);
    segments = data['segments'];

    printText();
}

// Get segments for target speaker
async function getTopSegments(topXname) {

    var colis = {
        "segList": statsSegments[topXname],
        "dataset": dataset
    }
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    }
    // ENVOI
    const response = await fetch('/_getTopSegments/', options);
    const data = await response.json();
    console.log(data);
    segments = data['segments'];

    printText();
}

function printText(){
    document.getElementById('textViz').innerHTML = "";

    segmentsName = []
    segments.forEach((seg)=>{
        if (seg.length>0) {
            segmentsName.push(seg[0]['file'].replace('.merged.pos_shape.TextGrid',''))
        }
    })

    var segsNbGreen = 0;
    var segsNbBlue = 0;
    var segsNbRed = 0;
    var segsNbPauses = 0;
    var segsNbTokens = 0;

    segments.forEach((seg)=>{
        var seghtml = "";
        var segFile = "";
        var segNbGreen = 0;
        var segNbBlue = 0;
        var segNbRed = 0;
        var segNbPauses = 0;
        var previousPauseEndTime = 0;
        seg.forEach((word)=>{
            segFile = word['file'];
            var fileId = segmentsName.indexOf(segFile);
            var pausehtml = ""

            if(word['duration']>pauseThreshold.min){
                segNbPauses++;
                var pauseType = ""
                var pauseTitle = "Inter-phrase"
                if (word['duration']>=pauseThreshold.max) {
                    pauseType = "largePause"
                    pauseTitle = "Large pause (skipped)"
                } else if (clauseTag.includes(word['wordLeftEndingLarger']) || clauseTag.includes(word['wordRightStartingLarger'])){
                    pauseType = "clauseBoundary"
                    pauseTitle = "Inter-clause"
                    segNbGreen++;
                } else if (conjTag.includes(word['wordLeftEndingLarger']) || conjTag.includes(word['wordRightStartingLarger'])){
                    pauseType = "conjBoundary"
                    pauseTitle = "Next to a conjunction"
                    segNbGreen++;
                } else if (!phraseTag.includes(word['wordLeftEndingLarger']) && !phraseTag.includes(word['wordRightStartingLarger'])){
                    pauseType = "wordBoundary"
                    pauseTitle = "Intra-phrase"
                    segNbRed++;
                } else {
                    segNbBlue++;
                }

                if (word['duration']>=pauseThreshold.max) {
                    pausehtml = `<div class='pause ${pauseType}' title='${pauseTitle}: ${parseFloat(word['duration']).toFixed(3)} sec.'>LargePause</div>`;
                } else {
                    pausehtml = `<div class='pause ${pauseType}' title='${pauseTitle}: ${parseFloat(word['duration']).toFixed(3)} sec.' style="width:${Math.round(word['duration']*100)}px">P</div>`;
                }
            }
            if(!(seghtml.length==0 && word['wordLeft']=="start")){
                seghtml += `<div class='word file${fileId}' data-start="${previousPauseEndTime}" data-end="${word['start']}" onclick="playWord(${fileId},${previousPauseEndTime}, ${word['start']})">${word['wordLeft']}</div>` + pausehtml;
            }
            previousPauseEndTime = word['end']
        })
        
        if (seg.length>0 && seg.length>=minNbTokens+1) {
            document.getElementById('textViz').innerHTML += 
            `<div class='segment'>
                <div class='d-flex align-items-center controlBar'>
                    <div>${segFile.replace('.merged.pos_shape.TextGrid','')}</div>
                    <div class="ms-2">(${Number(previousPauseEndTime).toFixed()}s. ${seg.length-1}tok.)</div>
                    <div class="ms-auto me-1">
                        P: ${segNbPauses}; 
                        <span class="pause clauseBoundary">P</span>=${segNbGreen}; 
                        <span class="pause wordBoundary">P</span>=${segNbRed};
                        <span class="pause clauseBoundary">P</span>/P=${(segNbGreen/segNbPauses).toFixed(2)};
                        <span class="pause wordBoundary">P</span>/P=${(segNbRed/segNbPauses).toFixed(2)}
                    </div>
                    <button class='btn btn-primary playSegmentBtn' type='button' onclick="playSegment(this, '${segFile.replace('.merged.pos_shape.TextGrid','')}')">Play <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-play-circle-fill" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5"/>
                  </svg></button>
                </div>
                <div class="d-flex flex-wrap segmentText">
                    ${seghtml}
                </div>
            </div>`
        }

        segsNbGreen += segNbGreen;
        segsNbBlue += segNbBlue;
        segsNbRed += segNbRed;
        segsNbPauses += segNbPauses;
        segsNbTokens += seg.length-1;
    })

    document.getElementById('stats_pauses_tokens').innerText = (segsNbPauses/segsNbTokens).toFixed(3);
    document.getElementById('stats_pClause_p').innerText = (segsNbGreen/segsNbPauses).toFixed(3);
    document.getElementById('stats_pWord_p').innerText = (segsNbRed/segsNbPauses).toFixed(3);
    document.getElementById('stats_pClause_tokens').innerText = (segsNbGreen/segsNbTokens).toFixed(3);
    document.getElementById('stats_pWord_tokens').innerText = (segsNbRed/segsNbTokens).toFixed(3);
}


// Run "Make stats on all segments"
var statsSegments = {};
async function makeSegmentsStats() {

    var colis = {
        "pauseThreshold": pauseThreshold,
        "speakersB1B2": locuteurs,
        "dataset": dataset,
        "minNbTokens" : minNbTokens
    }

    document.getElementById('topSegsBtn').innerHTML = ""

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    }
    // ENVOI
    const response = await fetch('/_makeSegmentsStats/', options);
    const data = await response.json();
    console.log(data);
    statsSegments = data;

    for (topXname in statsSegments){
        document.getElementById('topSegsBtn').innerHTML += `<button type="button" class="btn btn-primary m-1" onclick="getTopSegments('${topXname}')">${topXname}</button>`
    }
}




var audioFolder = "../static/audio/"+dataset
var audio = document.getElementById('audioPlayer');
var currentlyPlayingAudio = "";
var currentlyPlayingAudioId = "";

var audio = document.getElementById('audioPlayer');

function playSegment(thisBtn, file) {
    // Init all buttons to 'Play'
    stopPlaying();
    var newAudioSource = audioFolder + '/' + file + audioExt;
    letsPlay = false;
    if (audio.paused) {
        // CURRENTLY NOT PLAYING
        letsPlay = true;
    } else {
        // CURRENTLY PLAYING
        audio.pause();
        if (currentlyPlayingAudio != file) {
            letsPlay = true;
        }
    }

    if (letsPlay) {
        if (currentlyPlayingAudio != file) {
            audio.src = newAudioSource;
        }
        audio.play();
        thisBtn.innerHTML = `Pause <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pause-circle-fill" viewBox="0 0 16 16">
        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M6.25 5C5.56 5 5 5.56 5 6.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C7.5 5.56 6.94 5 6.25 5m3.5 0c-.69 0-1.25.56-1.25 1.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C11 5.56 10.44 5 9.75 5"/>
      </svg>`
    }
    currentlyPlayingAudio = file;
    currentlyPlayingAudioId = '.file'+segmentsName.indexOf(file)
}

function stopPlaying() {
    document.querySelectorAll('.playSegmentBtn').forEach((btnEl)=>{
        btnEl.innerHTML = `Play <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-play-circle-fill" viewBox="0 0 16 16">
        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5"/>
        </svg>`
    })
}

function playWord(fileId, deb, fin) {
    // var audioFolder = document.getElementById('audioFolder').value; // "../../../local/enregistrementsCLES/audio/"
    audio.pause()
    audio = new Audio()
    audio.addEventListener("timeupdate", showPlayedWord)
    audio.addEventListener("ended", stopPlaying)
    audio.src = audioFolder + '/' + segmentsName[fileId] + audioExt;
    audio.currentTime = deb;
    audio.play();
    console.log("./praatviz.sh",segmentsName[fileId],deb)
    setTimeout( () => { audio.pause() }, (fin-deb)*1000+50);
}

window.onkeydown = function(ev) {
    if(ev.keyCode == 32) {
        ev.preventDefault();
        audio.pause();
    }
}

// THIS FUNCTION IS TOO SLOW
// add ontimeupdate="showPlayedWord()" in audio player on the html page to use it.
function showPlayedWord() {
    document.querySelectorAll(currentlyPlayingAudioId).forEach((w) => {
        if (audio.currentTime >= w.dataset['start'] && audio.currentTime < w.dataset['end']){
            w.classList.add('active')
        } else {
            w.classList.remove('active')
        }
    })   
}