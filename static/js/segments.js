// Get specific request in URL if any
const urlParams = new URLSearchParams(window.location.search);
const req = urlParams.get('req')
if (req) {
    console.log("Request:",req);
    getSpeakerSegments(req);
}


function goToView(view) {
    if (view=="global") {
        window.location = '/'+dataset;
    } else if (view=="stress") {
        window.location = '/segments/'+dataset;
    } else if (view=="pauses") {
        if (["CLES"].includes(dataset)) {
            window.location = '/pausesviz/'+dataset;
        } else {
            window.alert("Pauses analysis has not been done yet on this dataset.")
        }
    }
}

function mean(arr){
    var sum = 0;
    for(var i in arr) {
        sum += arr[i];
    }
    return (sum / arr.length);
}

getSpeakers();


// Get list of speakers
var locuteurs = {};
async function getSpeakers() {

    var colis = {
        "dataset": dataset
    }
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    }
    // ENVOI
    const response = await fetch('/_getSpeakers/', options);
    const data = await response.json();
    console.log(data);
    locuteurs = data;

    document.getElementById('speakers').innerHTML = "<option value='none' selected>Select</option>";
    for (loc in locuteurs) {
        document.getElementById('speakers').innerHTML += `<option value="${locuteurs[loc]['speakerID']}">${locuteurs[loc]['speakerID']}</option>`
    }

    // makeSegmentsStatsStress()
}

var segments = [];
var segmentsName = [];

// Get segments for target speaker
async function getSpeakerSegments(loc) {
   
    var colis = {
        "speaker": loc,
        "dataset": dataset
    }
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    }
    // ENVOI
    const response = await fetch('/_getWordSegments/', options);
    const data = await response.json();
    console.log(data);
    segments = data['segments'];

    printText();
}

// Get segments from list
async function getTopSegments(topXname) {
   
    var colis = {
        "segList": statsSegments[topXname]
    }
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    }
    // ENVOI
    const response = await fetch('/_getTopSegments/', options);
    const data = await response.json();
    console.log(data);
    segments = data['segments'];

    // printText();
}

function printText(){
    document.getElementById('textViz').innerHTML = "";

    segmentsName = []
    segments.forEach((seg)=>{
        if (seg.length>0) {
            segmentsName.push(seg[0]['file'].replace('.merged.pos_shape.TextGrid',''))
        }
    })

    segments.forEach((seg)=>{
        var seghtml = "";
        var segFile = "";

        seg.forEach((word)=>{
            segFile = word['file'];
            var fileId = segmentsName.indexOf(segFile);

            if(word['ID']!=""){
                // Stressed word
                if (word['observedShape']==word['expectedShape']) {
                    stressClass = 'correct'
                } else {
                    stressClass = 'error'
                }
                var stressPos = 0; // position of stressed syllable (e.g. oOo = 2, Oo = 1)
                var cpt = 0 // syllable counter
                word['expectedShape'].split('').forEach((syll) => {
                    cpt++
                    if (syll == "O") stressPos = cpt;
                })

                var stressSyll = 0; // decile of expected stressed syllable
                var unstressSylls = []; // deciles of (expected) unstressed syllables
                var contrastView = ""
                var cpt = 0 // syllable counter
                JSON.parse(word['globalDeciles']).forEach((decile)=>{
                    cpt++
                    if (cpt==stressPos) {
                        stressSyll = decile
                    } else {
                        unstressSylls.push(decile)
                    }
                    decile = (decile/2).toFixed(0)
                    contrastView += `<div class="syll" style="width:${decile}px;height:${decile}px"></div>`
                })

                var meanUnstressed = mean(unstressSylls)
                // Compute stress score 1:  stressedSyllable / ( stressedSyllable + mean(unstressedSyllables))
                // var score1 = stressSyll / (stressSyll + meanUnstressed)
                // Compute stress score 2:  (stressedSyllable - mean(unstressedSyllables)) / ( stressedSyllable + mean(unstressedSyllables))
                var score2 = (stressSyll - meanUnstressed) / (stressSyll + meanUnstressed)

                seghtml += `<div style='position:relative' class='word stress ${stressClass} file${fileId}' data-start="${word['start']}" data-end="${word['end']}" onclick="playWord(${fileId},${word['start']}, ${word['end']})">
                                <div>${word['word']}</div>
                                <div class="d-flex justify-content-center align-items-center" title="Observed: ${word['observedShape']}">${contrastView}</div>
                                <div>(${word['expectedShape']})</div>
                                
                                <div style='position:absolute; bottom:-4px; right:0; font-size:.7em' title='(str-mean(unstr))/(str+mean(unstr))'>${score2.toFixed(2)}</div>
                            </div>`;
            } else {
                seghtml += `<div class='word file${fileId}' data-start="${word['start']}" data-end="${word['end']}" onclick="playWord(${fileId},${word['start']}, ${word['end']})">${word['word']}</div>`;
            }
            
        })
        
        if (seg.length>0) {
            document.getElementById('textViz').innerHTML += 
            `<div class='segment'>
                <div class='d-flex align-items-center controlBar'>
                    <div>${segFile.replace('.merged.pos_shape.TextGrid','')}</div>
                    <div class="ms-2">(${Number(seg[seg.length-1]['end']).toFixed()}s. ${seg.length}tok.)</div>
                    <div class="ms-auto me-1">
                        
                    </div>
                    <button class='btn btn-primary playSegmentBtn' type='button' onclick="playSegment(this, '${segFile.replace('.merged.pos_shape.TextGrid','')}')">Play <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-play-circle-fill" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5"/>
                  </svg></button>
                </div>
                <div class="d-flex flex-wrap segmentText">
                    ${seghtml}
                </div>
            </div>`
        }

        
    })
}


// Run "Make stats on all segments"
var statsSegments = {};
async function makeSegmentsStatsStress() {
    var colis = {
        "speakersB1B2": locuteurs
    }
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(colis)
    }
    // ENVOI
    const response = await fetch('/_makeSegmentsStats/', options);
    const data = await response.json();
    console.log(data);
    statsSegments = data;

    document.getElementById('topSegsBtn').innerHTML = ""
    for (topXname in statsSegments){
        document.getElementById('topSegsBtn').innerHTML += `<button type="button" class="btn btn-primary m-1" onclick="getTopSegments('${topXname}')">${topXname}</button>`
    }
}




var audioFolder = "../static/audio/"+dataset
var currentlyPlayingAudio = "";
var currentlyPlayingAudioId = "";

var audio = document.getElementById('audioPlayer');

function playSegment(thisBtn, file) {
    // Init all buttons to 'Play'
    stopPlaying();
    var newAudioSource = audioFolder + '/' + file + audioExt;
    letsPlay = false;
    if (audio.paused) {
        // CURRENTLY NOT PLAYING
        letsPlay = true;
    } else {
        // CURRENTLY PLAYING
        audio.pause();
        if (currentlyPlayingAudio != file) {
            letsPlay = true;
        }
    }

    if (letsPlay) {
        if (currentlyPlayingAudio != file) {
            audio.src = newAudioSource;
        }
        audio.play();
        thisBtn.innerHTML = `Pause <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pause-circle-fill" viewBox="0 0 16 16">
        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M6.25 5C5.56 5 5 5.56 5 6.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C7.5 5.56 6.94 5 6.25 5m3.5 0c-.69 0-1.25.56-1.25 1.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C11 5.56 10.44 5 9.75 5"/>
      </svg>`
    }
    currentlyPlayingAudio = file;
    currentlyPlayingAudioId = '.file'+segmentsName.indexOf(file)
}

function stopPlaying() {
    document.querySelectorAll('.playSegmentBtn').forEach((btnEl)=>{
        btnEl.innerHTML = `Play <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-play-circle-fill" viewBox="0 0 16 16">
        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5"/>
        </svg>`
    })
}

function playWord(fileId, deb, fin) {
    // var audioFolder = document.getElementById('audioFolder').value; // "../../../local/enregistrementsCLES/audio/"
    audio.pause()
    audio = new Audio()
    audio.addEventListener("timeupdate", showPlayedWord)
    audio.addEventListener("ended", stopPlaying)
    audio.src = audioFolder + '/' + segmentsName[fileId] + audioExt;
    audio.currentTime = deb;
    audio.play();
    console.log("./praatviz.sh",segmentsName[fileId],deb)
    setTimeout( () => { audio.pause() }, (fin-deb)*1000+50);
}

window.onkeydown = function(ev) {
    if(ev.keyCode == 32) {
        ev.preventDefault();
        audio.pause();
    }
}

// THIS FUNCTION IS TOO SLOW
// add ontimeupdate="showPlayedWord()" in audio player on the html page to use it.
function showPlayedWord() {
    document.querySelectorAll(currentlyPlayingAudioId).forEach((w) => {
        w.classList.remove('active')
        if (audio.currentTime >= w.dataset['start'] && audio.currentTime < w.dataset['end']){
            w.classList.add('active')
        }
    })   
}