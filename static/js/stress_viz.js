// Couleurs contrastées pour barchart nbword / nbsyll
const COLORS = [
    '#4dc9f6',
    '#f67019',
    '#f53794',
    '#537bc4',
    '#acc236',
    '#166a8f',
    '#00a950',
    '#58595b',
    '#8549ba'
  ];

// Vert vs rouge
const pieColors = [
    '#90ee90',
    '#f08080',
    '#f08080',
    '#f08080',
    '#f08080',
    '#f08080',
    '#f08080',
    '#f08080',
    '#f08080',
    '#f08080',
    '#f08080',
    '#f08080',
]

const color_gradient = {
    0: "#ffffff",
    1: "#fafafa",
    2: "#f5f5f5",
    3: "#eeeeee",
    4: "#e0e0e0",
    5: "#bdbdbd",
    6: "#757575",
    7: "#616161",
    8: "#424242",
    9: "#212121",
    10: "#101010"
}


const color_gradientOLD = {
    0: "#110094",
    1: "#6443b0",
    2: "#8667c0",
    3: "#af97d5",
    4: "#d8caea",
    5: "#cedbc9",
    6: "#9eb795",
    7: "#709563",
    8: "#417334",
    9: "#026300",
    10: "#025200"
}

$(document).ready(function () {
    $('#stressDetailTable').DataTable();
});


let csvLocuteurs = []; // liste complète des locuteurs
let locuteurs = {}; // dico complet des locuteurs
let selectedSpk = {}; // dico des locuteurs filtrés

let csvData = []; // liste complète des syllabes
let data = []; // liste des syllabes sans filtrage (état initial après chargement des données)
let datafiltre = []; // liste des syllabes filtrées

let allShapes = ["Oo","oO","oo", "OO", "Ooo","oOo","ooO","ooo","OOO","Oooo","oOoo","ooOo","oooO","Ooooo","oOooo","ooOoo","oooOo","ooooO"]; // toutes les shapes autorisées dans l'appli
let shapes = {'2':0, '3':0, '4':0, '5':0, '6':0}
let shape2real = {}
let shape2syll = []
var observedShapeWordList = [];

var nbWpluriBonNbSyll = 0
var nbExpectedIsObserved = 0;

let expand = true;
var limitTable = 299; // limite du nb de mots pour le tableau de mots
document.getElementById('limitTable').innerText=limitTable;

var audio;

function goToView(view) {
    if (view=="global") {
        window.location = '/'+dataset;
    } else if (view=="stress") {
        window.location = '/segments/'+dataset;
    } else if (view=="pauses") {
        if (["CLES"].includes(dataset)) {
            window.location = '/pausesviz/'+dataset;
        } else {
            window.alert("Pauses analysis has not been done yet on this dataset.")
        }
    }
}


// INITIATE CHARTS
makeCharts();

// LECTURE FICHIER LOCUTEURS


function loadData() {
    readCSVLocuteurs(rawCSVlocuteurs);
    readCSV(rawCSVstress);
}


/////////////////////////////////////////////////////////
// LECTURE DES FICHIERS

function readCSVLocuteurs(rawCSVlocuteurs) {
    
    csvLocuteurs = [];
    
    let lbreak = rawCSVlocuteurs.split("\n");
    header = lbreak[0].replace(/"/g,'').split(";");
    console.log("Header of the speaker file:"+header)
    lbreak = lbreak.slice(1,-1);
    lbreak.forEach(res => {
        let obj = {};
        let i = 0;
        for (col of res.split(";")){
            obj[header[i]] = col.replace(/"/g,'');
            i++
        }
        csvLocuteurs.push(obj);
    });
    csvLocuteurs.forEach((el)=>{
        locuteurs[el.speakerID] = el
    })
    console.log(csvLocuteurs.length+" speakers in the speaker file.")    
}

function readCSV(rawCSVstress) {
    
        csvData = [];
        
        let lbreak = rawCSVstress.split("\n");
        header = lbreak[0].split(";");
        console.log(header)
        var addsyllmfa_equals_syllnuclei = false
        if (!header.includes("syllmfa_equals_syllnuclei")){
            header.push('syllmfa_equals_syllnuclei')
            addsyllmfa_equals_syllnuclei = true
            console.log('Auto add column syllmfa_equals_syllnuclei (True)')
        }
        var adddebfin = false
        if (!header.includes('deb')) {
            // Fin de thèse: renommage de cetaines colones dans stressTable
            adddebfin = true
            header.push("deb")
            header.push("fin")
        }
        lbreak = lbreak.slice(1,-1);
        lbreak.forEach(res => {
            let obj = {};
            let i = 0;
            for (col of res.split(";")){
                obj[header[i]] = col;
                i++
            }
            if (addsyllmfa_equals_syllnuclei) obj['syllmfa_equals_syllnuclei'] = "True";
            if (adddebfin) {
                obj['deb'] = obj['startTime']
                obj['fin'] = obj['endTime']
            }
            csvData.push(obj);
        });
        console.log(csvData);
        setLocuteursSettings();
        setWordSettings();
        updateSpeaker();
        
}


///////////////////////////////////////////////////
// INITIALISATION DES FILTRES DISPONIBLES
// appelé seulement lors du chargement des fichiers csv

function setLocuteursSettings() {
    let cles = {}
    let clesIo = {}
    let mother = {}
    let gender = {}
    selectedSpk = {}

    // Liste des valeurs possibles
    csvLocuteurs.forEach((el)=>{
        // update cles list
        if ("CLES" in el) {
            if (!Object.keys(cles).includes(el.CLES)) cles[el.CLES] = 1
            else cles[el.CLES]++
        }

        // update clesIO list
        if ("CLES_IO" in el) {
            if (!Object.keys(clesIo).includes(el.CLES_IO)) clesIo[el.CLES_IO] = 1
            else clesIo[el.CLES_IO]++
        }

        // update mother list
        if ("L1" in el) {
            if (!Object.keys(mother).includes(el.L1)) mother[el.L1] = 1
            else mother[el.L1]++
        }

        // update gender list
        if ("Gender" in el) {
            if (!Object.keys(gender).includes(el.Gender)) gender[el.Gender] = 1
            else gender[el.Gender]++
        }

        // update selectedSpk
        if (!Object.keys(selectedSpk).includes(el.speakerID)) selectedSpk[el.speakerID] = 0
    })
    

    // Affichage des checkboxes
    // Liste des checkboxes CLES
    var items = Object.keys(cles).map(function(key) { return [key, cles[key]]; });
    items.sort(function(first, second) { return second[1] - first[1]; });
    
    let clesDiv = document.getElementById('cleslist');
    clesDiv.innerHTML = "";
    for (let x of items) {
        clesDiv.innerHTML += `<div class="form-check">
        <input class="form-check-input" type="checkbox" value="${x[0]}" id="${x[0]}" onchange="updateSpeaker()" checked>
        <label class="form-check-label" for="${x[0]}"><span class="filterName">${x[0]}</span>&emsp;(<span class="filteredNb">${x[1]}</span>/${x[1]})</label>
    </div>`
    }

    // Liste des checkboxes CLESIO
    var items = Object.keys(clesIo).map(function(key) { return [key, clesIo[key]]; });
    items.sort(function(first, second) { return second[1] - first[1]; });
    
    let clesIoDiv = document.getElementById('clesIolist');
    clesIoDiv.innerHTML = "";
    for (let x of items) {
        clesIoDiv.innerHTML += `<div class="form-check">
        <input class="form-check-input" type="checkbox" value="${x[0]}" id="clesIo${x[0]}" onchange="updateSpeaker()" checked>
        <label class="form-check-label" for="clesIo${x[0]}"><span class="filterName">${x[0]}</span>&emsp;(<span class="filteredNb">${x[1]}</span>/${x[1]})</label>
    </div>`
    }

    // Liste des checkboxes MOTHER
    var items = Object.keys(mother).map(function(key) { return [key, mother[key]]; });
    items.sort(function(first, second) { return second[1] - first[1]; });
    
    let motherDiv = document.getElementById('motherlist');
    motherDiv.innerHTML = "";
    for (let x of items) {
        motherDiv.innerHTML += `<div class="form-check">
        <input class="form-check-input" type="checkbox" value="${x[0]}" id="${x[0]}" onchange="updateSpeaker()" checked>
        <label class="form-check-label" for="${x[0]}"><span class="filterName">${x[0]}</span>&emsp;(<span class="filteredNb">${x[1]}</span>/${x[1]})</label>
    </div>`
    }

    // Liste des checkboxes GENDER
    var items = Object.keys(gender).map(function(key) { return [key, gender[key]]; });
    items.sort(function(first, second) { return second[1] - first[1]; });
    
    let genderDiv = document.getElementById('genderlist');
    genderDiv.innerHTML = "";
    for (let x of items) {
        genderDiv.innerHTML += `<div class="form-check">
        <input class="form-check-input" type="checkbox" value="${x[0]}" id="${x[0]}" onchange="updateSpeaker()" checked>
        <label class="form-check-label" for="${x[0]}"><span class="filterName">${x[0]}</span>&emsp;(<span class="filteredNb">${x[1]}</span>/${x[1]})</label>
    </div>`
    }
}

function setWordSettings() {
    // Filter DATA
    data = [];
    var spk = document.getElementById('speakers').value;  
    console.log("Génération de data pour spk=",spk);
    csvData.forEach((el)=>{
        if (allShapes.includes(el.expectedShape) && allShapes.includes(el.observedShape)) {
            if (spk == "all"){
                if (Object.keys(selectedSpk).includes(el.spk)) data.push(el);
            } else {
                if (el.spk==spk) data.push(el);
            }
        }
    })
    
    let poss = {}
    let shapes = {} // DOUBLE DÉCLARATION!!!

    data.forEach((el)=>{
        // update POS list
        if (!Object.keys(poss).includes(el.pos)) poss[el.pos] = 1
        else poss[el.pos]++

        // update Shape list
        if (!Object.keys(shapes).includes(el.expectedShape)) shapes[el.expectedShape] = 1
        else shapes[el.expectedShape]++
    })
    

    // Liste des checkboxes POS
    var items = Object.keys(poss).map(function(key) { return [key, poss[key]]; });
    items.sort(function(first, second) { return second[1] - first[1]; });

    let posDiv = document.getElementById('poslist');
    posDiv.innerHTML = "";
    for (let pos of items) {
        posDiv.innerHTML += `<div class="form-check">
        <input class="form-check-input" type="checkbox" value="${pos[0]}" id="${pos[0]}" onchange="updateWord()" checked>
        <label class="form-check-label" for="${pos[0]}"><span class="filterName">${pos[0]}</span>&emsp;(<span class="filteredNb">${pos[1]}</span>/${pos[1]})</label>
    </div>`
    }

    // Liste des checkboxes Shapes
    var items = Object.keys(shapes).map(function(key) { return [key, shapes[key]]; });
    items.sort(function(first, second) { return second[1] - first[1]; });

    let shapeDiv = document.getElementById('shapelist');
    shapeDiv.innerHTML = "";
    for (let shape of items) {
        shapeDiv.innerHTML += `<div class="form-check">
        <input class="form-check-input" type="checkbox" value="${shape[0]}" id="${shape[0]}" onchange="updateWord()" checked>
        <label class="form-check-label" for="${shape[0]}"><span class="filterName">${shape[0]}</span>&emsp;(<span class="filteredNb">${shape[1]}</span>/${shape[1]})</label>
    </div>`
    }
}

// SHORTCUT FUNCTIONS

function selectOnly(cblist, level) {
    document.getElementById(cblist).childNodes.forEach((el)=>{
        el.children[0].checked = false;
    })
    document.getElementById(level).checked = true;
    updateSpeaker();
}

///////////////////////////////////////////////////////////////////////////////////////////
// MISE À JOUR DES FILTRES ET DES VISUELS

function getListOfIdFromCheckboxList(checkboxDivId) {
    let list = []
    document.getElementById(checkboxDivId).childNodes.forEach((el)=>{
        if (el.childNodes[1].checked) list.push(el.childNodes[1].value);
    })
    return list
}

function updateCheckBoxInfo(checkboxDivId, filterNb){
    // Met à jout le nombre d'entrées par filtre
    document.getElementById(checkboxDivId).childNodes.forEach((el)=>{
        if (Object.keys(filterNb).includes(el.childNodes[3].children[0].innerText)) {
            el.childNodes[3].classList.remove('filtered0')
            el.childNodes[3].children[1].innerText = filterNb[el.childNodes[3].children[0].innerText]
        } else {
            el.childNodes[3].classList.add('filtered0')
            el.childNodes[3].children[1].innerText = 0
        }
    })
}

function updateSpeaker() {
    // Speaker Filters
    let cless = getListOfIdFromCheckboxList('cleslist')
    let clesIos = getListOfIdFromCheckboxList('clesIolist')
    let mothers = getListOfIdFromCheckboxList('motherlist')
    let genders = getListOfIdFromCheckboxList('genderlist')

    if (document.getElementById('speakers').value == "all") {
        // Select speakers
        selectedSpk = {} // réinitialisation du dico de locuteurs
        csvData.forEach((el)=>{
            if (Object.keys(locuteurs).includes(el.spk) &&
                (!("CLES" in csvLocuteurs[0]) | cless.includes(locuteurs[el.spk].CLES)) && 
                (!("CLES_IO" in csvLocuteurs[0]) | clesIos.includes(locuteurs[el.spk].CLES_IO)) && 
                (!("L1" in csvLocuteurs[0]) | mothers.includes(locuteurs[el.spk].L1)) && 
                (!("Gender" in csvLocuteurs[0]) | genders.includes(locuteurs[el.spk].Gender)) 
            ) {
                if (!Object.keys(selectedSpk).includes(el.spk)) selectedSpk[el.spk] = 1
                else selectedSpk[el.spk]++
            }
        })
        // update speaker menu
        document.getElementById('speakers').innerHTML = `<option value="all" selected>All&emsp;(${ Object.keys(selectedSpk).length } speakers)</option>`;
        for (let spk of Object.keys(selectedSpk).sort()) {
            document.getElementById('speakers').innerHTML += `<option value="${spk}">${spk}&emsp;(${selectedSpk[spk]} words)</option>`
        }
    } else {
        selectedSpk = {}
        selectedSpk[document.getElementById('speakers').value] = 0
    }

    // update checkbox info
    let clesNb = {}
    let clesIoNb = {}
    let motherNb = {}
    let genderNb = {}
    csvLocuteurs.forEach((el)=>{
        if (Object.keys(selectedSpk).includes(el.speakerID)) {
            if (!Object.keys(clesNb).includes(el.CLES)) clesNb[el.CLES] = 1
            else clesNb[el.CLES]++
            if (!Object.keys(clesIoNb).includes(el.CLES_IO)) clesIoNb[el.CLES_IO] = 1
            else clesIoNb[el.CLES_IO]++
            if (!Object.keys(motherNb).includes(el.L1)) motherNb[el.L1] = 1
            else motherNb[el.L1]++
            if (!Object.keys(genderNb).includes(el.Gender)) genderNb[el.Gender] = 1
            else genderNb[el.Gender]++
        }
    })

    updateCheckBoxInfo('cleslist', clesNb);
    updateCheckBoxInfo('clesIolist', clesIoNb);
    updateCheckBoxInfo('motherlist', motherNb);
    updateCheckBoxInfo('genderlist', genderNb);

    updateWord()
}



function updateWord() {
    // checkbox: keep only words in which MFA syllable count matches Syllable-nuclei count
    var mfaSyll = ".*";
    if (document.getElementById('filterMFA').checked==true) mfaSyll = "True"

    // Syllable filters
    let poss = getListOfIdFromCheckboxList('poslist')
    let shapesSelected = getListOfIdFromCheckboxList('shapelist')
    let filterByWord = document.getElementById('filterByWord').value;
    filterByWord = new RegExp(filterByWord, 'i'); // ignore case

    // parse data table
    nbWpluriBonNbSyll = 0
    shape2real = {}
    for (x of ["Oo","oO","Ooo","oOo","ooO","Oooo","oOoo","ooOo","oooO"]) {
        shape2real[x]= {}
    }

    // update checkbox info
    let posNb = {}
    let shapeNb = {}
    nbSyll = {'2':0, '3':0, '4':0, '5':0, '6':0}
    nbExpectedIsObserved = 0;

    datafiltre = []
    data.forEach((el)=>{
        if (Object.keys(selectedSpk).includes(el.spk) && poss.includes(el.pos) && shapesSelected.includes(el.expectedShape) && el.lab.match(filterByWord) && el.syllmfa_equals_syllnuclei.match(mfaSyll)) {
            datafiltre.push(el);
            nbWpluriBonNbSyll++;
            if (Object.keys(nbSyll).includes(el.lenSyllxpos)) nbSyll[el.lenSyllxpos]++
            
            if (!Object.keys(shape2real).includes(el.expectedShape)) shape2real[el.expectedShape] = {}
            if (!Object.keys(shape2real[el.expectedShape]).includes(el.observedShape)) shape2real[el.expectedShape][el.observedShape] = 0
            shape2real[el.expectedShape][el.observedShape] += 1

            if (!Object.keys(posNb).includes(el.pos)) posNb[el.pos] = 1
            else posNb[el.pos]++
            if (!Object.keys(shapeNb).includes(el.expectedShape)) shapeNb[el.expectedShape] = 1
            else shapeNb[el.expectedShape]++

            if (el.expectedShape == el[xShape]) nbExpectedIsObserved++;
        }
    })

    document.querySelectorAll('.nbWpluriBonNbSyll').forEach((span)=> {
        span.innerText = nbWpluriBonNbSyll;
    })
    document.getElementById('nbExpectedIsObserved').innerText = nbExpectedIsObserved;
    document.getElementById('percentExpectedIsObserved').innerText = (nbExpectedIsObserved*100/nbWpluriBonNbSyll).toFixed(0);
    updateCheckBoxInfo('poslist', posNb);
    updateCheckBoxInfo('shapelist', shapeNb);

    updateCharts()
}
var xShape = "observedShape"

function toggleExpand() {
    expand = !expand
    updateWord()
}

function toggleAllCheckboxes(cat) {
    let someAreEmpty = false;
    document.getElementById(cat+'list').childNodes.forEach((el)=>{ if (el.childNodes[1].checked == false) someAreEmpty=true})
    if (someAreEmpty) {
        // check all
        console.log("Some are empty: check all")
        document.getElementById(cat+'list').childNodes.forEach((el)=>{ el.childNodes[1].checked = true })
    } else {
        // uncheck all
        console.log("uncheck all")
        document.getElementById(cat+'list').childNodes.forEach((el)=>{ el.childNodes[1].checked = false })
    }

    if (['cles','clesIo','mother','gender'].includes(cat)) updateSpeaker();
    else updateWord();
}

var chartNbSyll;
var chartObsOo;
var chartObsoO;
var chartObsOoo;
var chartObsoOo;
var chartObsooO;
var chartObsOooo;
var chartObsoOoo;
var chartObsooOo;
var chartObsoooO;


function makeCharts() {
    console.log('Initialisation des graphiques...')
    chartNbSyll = makeChart('chartNbSyll', {'2':0, '3':0, '4':0, '5':0, '6':0}, type='bar', colors=COLORS);

    chartObsOK = makeChart('chartObsOK', {}, type='pie', colors=pieColors);

    chartObsOo = makeChart('chartObsOo', {}, type='pie', colors=pieColors);
    chartObsoO = makeChart('chartObsoO', {}, type='pie', colors=pieColors);
    chartObsOoo = makeChart('chartObsOoo', {}, type='pie', colors=pieColors);
    chartObsoOo = makeChart('chartObsoOo', {}, type='pie', colors=pieColors);
    chartObsooO = makeChart('chartObsooO', {}, type='pie', colors=pieColors);
    chartObsOooo = makeChart('chartObsOooo', {}, type='pie', colors=pieColors);
    chartObsoOoo = makeChart('chartObsoOoo', {}, type='pie', colors=pieColors);
    chartObsooOo = makeChart('chartObsooOo', {}, type='pie', colors=pieColors);
    chartObsoooO = makeChart('chartObsoooO', {}, type='pie', colors=pieColors);
}

function updateCharts() {
    upd(chartNbSyll,nbSyll)

    upd(chartObsOK, { "OK": nbExpectedIsObserved, "X": nbWpluriBonNbSyll-nbExpectedIsObserved });

    upd(chartObsOo, shape2real['Oo']);
    upd(chartObsoO, shape2real['oO']);
    upd(chartObsOoo, shape2real['Ooo']);
    upd(chartObsoOo, shape2real['oOo']);
    upd(chartObsooO, shape2real['ooO']);
    upd(chartObsOooo, shape2real['Oooo']);
    upd(chartObsoOoo, shape2real['oOoo']);
    upd(chartObsooOo, shape2real['ooOo']);   
    upd(chartObsoooO, shape2real['oooO']);

    updateObservedVsExpectedCharts();
    getStressDetails(document.getElementById('stressDetailSelect').value)
}


function upd(chart, s) {
    items = Object.keys(s).map(function(key) { return [key, s[key]]; });
    items.sort(function(first, second) { return  first[0] - second[0]; });

    if (chart.canvas.id.includes('chartObs')) {
        // On met la shape de chart en premier et les autres à la suite
        var headShape = chart.canvas.id.replace('chartObs','')
        var headShapeFreq = []
        for (x=0;x<items.length;x++) {
            if (items[x][0]==headShape) {
                headShapeFreq = items[x]
                items.splice(x,1)
            }
        }
        var newItems = [headShapeFreq].concat(items)
        items = newItems
    }

    // chart.data.labels = s.map((k)=>{return k[0]})
    // chart.data.datasets[0].data = s.map((k)=>{return k[1]})
    chart.data.labels = items.map((k)=>{return k[0]})
    chart.data.datasets[0].data = items.map((k)=>{return k[1]})
    
    chart.update();
}


function makeChart(canvasName, stats, type="bar", colors=COLORS) {
    var data = {
        labels: Object.keys(stats),
        datasets: [{
        label: canvasName,
        data: Object.values(stats),
        backgroundColor: colors,
        hoverOffset: 4
        }]
    };
    
    // ChartJS plugin Labels : https://github.com/emn178/chartjs-plugin-labels
    if (canvasName == "chartNbSyll") var renderValue = 'value'
    else var renderValue = 'percentage'

    const config = {
        type: type,
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: true,
            legend: {
                display: false
            },
            plugins: {
                legend: {
                    display: false
                },
                labels: {
                        render: renderValue
                }
            }
        }
    };
    
    const myChart = new Chart(
        document.getElementById(canvasName),
        config
    );

    return myChart
}

function updateObservedVsExpectedCharts() {
    // EXPECTED vs OBSERVED shapes
    document.querySelectorAll('.obs').forEach((el)=>{ el.style.display = "none" })
    let chartHeight = 500 //document.querySelector('#chartPosition').offsetHeight;
    let max = 0
    let localMaxs = {}
    for (i in shape2real) {
        let localMax = 0
        for (j in shape2real[i]) {
            localMax+=shape2real[i][j]
        }
        localMaxs[i] = localMax;
        if (localMax > max) max = localMax;
    }
    for (i in shape2real) {
        if (["Oo","oO","Ooo","oOo","ooO","Oooo","oOoo","ooOo","oooO"].includes(i)) {
            if (expand) { max = localMaxs[i] }
            for (j in shape2real[i]) {
                document.querySelector('.exp'+i+'.obs'+j).style.display = "block";
                document.querySelector('.exp'+i+'.obs'+j).style.height = shape2real[i][j] *(chartHeight-100)/max +"px";
                document.querySelector('.exp'+i+'.obs'+j).innerText = j + ` (${shape2real[i][j]})`;
                document.querySelector('.exp'+i+'.obs'+j).title = j + ` (${shape2real[i][j]})`;
            }
        }
    }
}



function meanGlobalDeciles(shapeOo, colName) {
    // pour une shape donnée (ex. "oOoo"), renvoie une moyenne des déciles de chaque syllabe + nb d'occurrences
    var table = [];
    observedShapeWordList = [];
    
    var tgtshape = document.getElementById('shapeDetailSelect').value
    
    datafiltre.forEach((el) => {
        if (el[tgtshape]==shapeOo) {
            var cents = string2list(el[colName])
            var decs = [];
            cents.forEach((c)=>{ decs.push(c) }) // c/10 pour déciles au lieu de centiles
            table.push(decs);
            observedShapeWordList.push(el);
        }
    })
    
    var medians = []
    var sylldecs = []
    var sums = []
    
    for (i=0;i<shapeOo.length;i++) {
        sylldecs.push([])
        sums.push(0)
    }

    var tableLength = 0
    table.forEach((el)=>{
        tableLength++;
        for (i=0;i<el.length;i++) {
            sylldecs[i].push(el[i])
            sums[i]+=el[i]
        }
    })
    sums.forEach((el)=>{
        medians.push( Number((el/tableLength)).toFixed() ) //Math.ceil(el/table.length)-1)  // .toFixed() pour arrondir à l'entier
    })

    return [medians, tableLength]
}

function resetStressDetails() {
    ["globalDeciles","F0Deciles","dBDeciles","durDeciles"].forEach((el)=>{
        updateSyllRealisation([0,0,0,0,0],0,el)
        updateSyllRealisation([0,0],0,el+"Oo")
    })
}


function getMeansAndStrUnstr(shapeOo, colName) {
    // Pour une shape donnée (ex. "ooO") et une colonne de déciles (ex. globalDeciles),
    // renvoie 
            // - la liste des déciles moyens par syllabe, 
            // - le décile moyen O, 
            // - la moyenne des déciles o, 
            // - la position de O (ici 2), 
            // - le nombre d'occurrences
    var [medians, nbWords] = meanGlobalDeciles(shapeOo, colName)
    var stressed = 0;
    var stressId = 0;
    var unstresseds = [];
    var sum = 0;
    [...shapeOo].forEach((o,i) => {
        if (o=="O") {
            stressed = medians[i]
            stressId = i
        } else {
            unstresseds.push(medians[i])
            sum+=Number(medians[i])
        }
    })
    // MOYENNE DES UNSTRESSED pour avoir la moyenne unstressed générale
    var unstressed = Number( sum/unstresseds.length ).toFixed() // Math.ceil( sum/unstresseds.length )-1

    return [medians, stressed, unstressed, stressId, nbWords]
}

function getStressDetails(shapeOo) {
    resetStressDetails();
    document.getElementById('stressDetailNbSyll').innerText = "-";

    ["globalDeciles","F0Deciles","dBDeciles","durDeciles"].forEach((colName)=>{
        if (shapeOo!='all') {
            var [medians, stressed, unstressed, stressId, nbWords] = getMeansAndStrUnstr(shapeOo, colName)
            console.log(shapeOo, colName, medians, stressed, unstressed, stressId, nbWords)

            updateSyllRealisation(medians, stressId, colName)
            updateSyllRealisation([unstressed,stressed], 1, colName+'Oo')
            document.getElementById('stressDetailNbSyll').innerText = nbWords

        } else {
            var triplets = []; // list of [nbWords,decileStressed,decileUnstressed]
            allShapes.forEach( (s) => {
                var [medians, stressed, unstressed, stressId, nbWords] = getMeansAndStrUnstr(s, colName)
                if (stressed>=0 && unstressed>=0) triplets.push([nbWords, stressed, unstressed])
            })
            var sumsWithCoef = [0,0,0];
            triplets.forEach((t)=>{
                sumsWithCoef[0] += t[0]
                sumsWithCoef[1] += t[0]*t[1]
                sumsWithCoef[2] += t[0]*t[2]
            })
            var stressed = Number( sumsWithCoef[1]/sumsWithCoef[0] ).toFixed()
            var unstressed = Number( sumsWithCoef[2]/sumsWithCoef[0] ).toFixed()
            updateSyllRealisation([unstressed,stressed], 1, colName+'Oo')
            document.getElementById('stressDetailNbSyll').innerText = sumsWithCoef[0]
        }
    })
    makeTable()
}


function updateSyllRealisation(valList,stressId, dim) {
    valList.forEach((el,ind)=>{
        if (el>=0) {
            eldec = (el/10).toFixed();
            document.getElementById('syll'+(ind+1)+dim).style.width = 2000*eldec/100+"px" 
            document.getElementById('syll'+(ind+1)+dim).style.height = 2000*eldec/100+"px" 
            if (eldec==0) document.getElementById('syll'+(ind+1)+dim).innerText = "";
            else document.getElementById('syll'+(ind+1)+dim).innerText = el;

            if (stressId==ind) {
                document.getElementById('syll'+(ind+1)+dim).style.borderColor = "green";
                document.getElementById('syll'+(ind+1)+dim).style.backgroundColor = color_gradient[eldec];
            } else { 
                document.getElementById('syll'+(ind+1)+dim).style.borderColor = "black";
                document.getElementById('syll'+(ind+1)+dim).style.backgroundColor = color_gradient[eldec];
            }
            
        }
    })
}

function string2list(str) {
    var valList = [];
    str.replace(/\[|\]/g,'').split(',').forEach((v)=>{ valList.push(Number(v.trim())) });
    return valList
}

function mean(list) {
    var sum = 0;
    list.forEach( (val) => { sum+=val } )
    return sum/list.length
}

function deciles2shape(cell) {
    var valList = string2list(cell.innerText);
    cell.innerHTML = "<div class='d-flex align-items-center'></div>";
    for (v in valList) cell.children[0].innerHTML += `<div class="d-flex justify-content-center align-items-center" style="border:solid 1px black; border-radius: 50%;width:0px; height: 0px;"></div>`
    
    valList.forEach((el100, ind)=>{
        el = (el100/10).toFixed(0); // DANS LE CAS DE CENTILES + arrondi à 0 décimale
        // el = Math.floor(el100/10); // DANS LE CAS DE CENTILES + arrondi inférieur à 0 décimale
        cell.children[0].children[ind].style.width = el*4+10+"px" 
        cell.children[0].children[ind].style.height = el*4+10+"px" 
        if (el==0) cell.children[0].children[ind].innerText = "";
        else cell.children[0].children[ind].innerText = el;
        cell.children[0].children[ind].style.backgroundColor = color_gradient[el];
    })
}

var audio = document.getElementById('audioPlayer');
var deb = 0.0
var fin = 0.0
var before = 0.0
var after = 0.0



var audio = document.getElementById('audioPlayer');

function playWord(file, deb, fin) {
    // var audioFolder = document.getElementById('audioFolder').value; // "../../../local/enregistrementsCLES/audio/"
    audio.src = audioFolder + '/' + file.replace('.merged.pos','').replace('.TextGrid','') + audioExt;
    audio.currentTime = deb;
    audio.play();
    console.log("./praatviz.sh",file,deb)
    setTimeout( () => { audio.pause() }, (fin-deb)*1000+50);
}

function playWordLarge(audioId, before=4, after=4) {
    // var audioFolder = document.getElementById('audioFolder').value; // "../../../local/enregistrementsCLES/audio/"
    for (w of datafiltre) {
        if (w.ID==audioId) {
            var ddeb = +(w.deb)
            var ffin = +(w.fin)
            console.log("Trouvé!", w.file, ddeb, ffin)
            audio.src = audioFolder + '/' + w.file.replace('.merged.pos','').replace('.TextGrid','') + audioExt;
            // audio = new Audio("../../../local/enregistrementsCLES/audio/"+w.file.replace(".merged.pos.TextGrid",".wav"));
            var temps_debut = 0;
            if (ddeb-before>=0) {
                temps_debut = ddeb-before;
            }
            console.log("Lecture",temps_debut,ffin+after)
            audio.currentTime = temps_debut
            audio.play();
            setTimeout( () => { audio.pause() }, (ffin+after-(temps_debut))*1000);
        }
    }
}



function playWordX(file, xdeb, xfin) {
    audio.src = audioFolder + '/' + file.replace('.merged.pos','').replace('.TextGrid','') + audioExt;
    deb = xdeb
    fin = xfin
    
    audio.currentTime = deb;
    audio.play();
    setTimeout( () => { audio.pause() }, (fin-deb)*1000+50);
    
    // audio.addEventListener('canplaythrough', function() { 
        
    //  }, false);
    // audio.play();
    // console.log("./praatviz.sh",file,deb)
}

function playWordLargeX(audioId, xbefore=4, xafter=4) {
    for (w of datafiltre) {
        if (w.ID==audioId) {
            deb = +(w.deb)
            fin = +(w.fin)
            before = xbefore;
            after = xafter;
            console.log("Trouvé!", w.file, deb, fin)
            audio.src = audioFolder + '/' + w.file.replace('.merged.pos','').replace('.TextGrid','') + audioExt;
            // audio = new Audio("../../../local/enregistrementsCLES/audio/"+w.file.replace(".merged.pos.TextGrid",".wav"));
            
            var temps_debut = 0;
            if (deb-before>=0) {
                temps_debut = deb-before;
            }
            audio.currentTime = temps_debut
            console.log("Lecture",temps_debut,fin+after)
            audio.play();
            // setTimeout( () => { audio.pause() }, (fin+after-(temps_debut))*1000);

            // audio.addEventListener('canplaythrough', function() { 
            //     var temps_debut = 0;
            //     if (deb-before>=0) {
            //         temps_debut = deb-before;
            //     }
            //     audio.currentTime = temps_debut
            //     console.log("Lecture",temps_debut,fin+after)
            //     audio.play();
            //     // setTimeout( () => { audio.pause() }, (fin+after-(temps_debut))*1000);
            //  }, false);
            // audio.play();
            // setTimeout( () => { audio.pause() }, (ffin+after-(temps_debut))*1000);
        }
    }
}

function makeTable() {
    var currentShape = document.getElementById('stressDetailSelect').value;
    var currentComp = document.getElementById('shapeDetailSelect').value;

    document.getElementById('stressDetailTableDiv').innerHTML = `<table id="stressDetailTable" class="hover" data-order='[[ 1, "asc" ]]' data-page-length='25'>
    <thead>
        <tr>
            <th>Speaker</th>
            <th>Word</th>
            <th>POS</th>
            <th>Expected</th>
            <th>Observed</th>
            <th>Pitch</th>
            <th>Energy</th>
            <th>Duration</th>
        </tr>
    </thead>
    <tbody id="stressDetailTableBody">

    </tbody>
</table>`;

    
    var tbody = document.getElementById('stressDetailTableBody');
    var cpt = 0;
    for (w of datafiltre) {
        if (currentShape == "all" || w[currentComp] == currentShape) {
            cpt++;
            if (cpt <= limitTable) {
                if (w.expectedShape == w.observedShape) { 
                    var cc = "sameShape"
                } else {
                    var cc = "diffShape"
                }
                tbody.innerHTML += `<tr>
                <td class="pointer" title="Click to play this word context" onclick="playWordLarge('${w.ID}')">${w.spk}</td>
                <td class="pointer" title="Click to play this word" onclick="showRow(${cpt-1});playWord('${w.file.replace('.TextGrid','').replace('.merged.pos','')}', ${w.deb}, ${w.fin})">${w.lab}</td>
                <td>${w.pos}</td>
                <td class="${cc}">${w.expectedShape}</td>
                <td class="${cc}" title="${w.globalDeciles}">${w.observedShape}</td>
                <td class="deciles">${w.F0Deciles}</td>
                <td class="deciles">${w.dBDeciles}</td>
                <td class="deciles">${w.durDeciles}</td>
            </tr>`
            }
        }
    }

    document.querySelectorAll('.deciles').forEach((el)=>{deciles2shape(el)});
    

    $('#stressDetailTable').DataTable();
}

function showRow(rowNb) {
    console.log(datafiltre[rowNb]);
}




//////////////////////////////////////////////
/////// STATS PONCTUELLES
////

function tempFu() {
    var diffList = [];
    data.forEach((el)=> {
        var stressedOn = -1;
        for (o in el.observedShape) {
            if (el.observedShape[o]=="O") stressedOn = o
        }

        var stressedOnDec = -1;
        var valList = [];
        el.globalDeciles.replace(/\[|\]/g,'').split(',').forEach((v)=>{ valList.push(Number(v.trim())) });
        
        var stressedOnDec = valList.indexOf(Math.max(...valList))
        
        if (stressedOn != stressedOnDec) {
            // console.log(stressedOn,stressedOnDec, el.observedShape, el.globalDeciles)
            diffList.push(el);
        }
    })
    console.log(diffList);
}

var spkratediff = [];
function getStatPerSpk() {
    // Filtrer depuis l'interface en amont;
    // Cette fonction loop sur datafiltre
    // PER SPEAKER:
    //      Compute rate of words correctly shaped
    //      Compute multidimensional (o/O)
    //      Compute mean Pitch (o/O)
    //      Compute mean Energy (o/O)
    //      Compute mean Duration (o/O)
    //
    //   OBSERVED SHAPE

    var spk2stats = {};
    spkratediff = [];

    datafiltre.forEach((el)=>{
        if (!Object.keys(spk2stats).includes(el.spk)) {
            spk2stats[el.spk] = {
                spkId : el.spk,
                clesIo: locuteurs[el.spk].CLES_IO,
                nbExpIsObs : 0,     // nb of words correctly shaped
                nbW : 0,            // nb of words
                rateExpIsObs : 0,   // rate of words correctly shaped
                obsShape2dec: {},   // Multidimensional (o/O)
                obsShape2decF0: {}, // Mean Pitch (o/O)
                obsShape2decdB: {}, // Mean Energy (o/O)
                obsShape2decdur: {},// Mean Duration (o/O)
                diffdec: 0,         // difference O-o
                diffdecF0: 0,       // difference O-o F0
                diffdecdB: 0,       // difference O-o dB
                diffdecdur: 0,      // difference O-o dur
            }
        }

        spk2stats[el.spk].nbW++;
        if (el.expectedShape==el.observedShape) spk2stats[el.spk].nbExpIsObs++;

        if (!Object.keys(spk2stats[el.spk].obsShape2dec).includes(el.observedShape)) {
            spk2stats[el.spk].obsShape2dec[el.observedShape] = []
            spk2stats[el.spk].obsShape2decF0[el.observedShape] = []
            spk2stats[el.spk].obsShape2decdB[el.observedShape] = []
            spk2stats[el.spk].obsShape2decdur[el.observedShape] = []
        }
        spk2stats[el.spk].obsShape2dec[el.observedShape].push(el.globalDeciles)
        spk2stats[el.spk].obsShape2decF0[el.observedShape].push(el.F0Deciles)
        spk2stats[el.spk].obsShape2decdB[el.observedShape].push(el.dBDeciles)
        spk2stats[el.spk].obsShape2decdur[el.observedShape].push(el.durDeciles)
    })

    for (spk in spk2stats) {
        spk2stats[spk].rateExpIsObs = (spk2stats[spk].nbExpIsObs*100/spk2stats[spk].nbW).toFixed(2)

        var triplet = getNbw_µO_µo(spk2stats[spk].obsShape2dec);
        var tripletF0 = getNbw_µO_µo(spk2stats[spk].obsShape2decF0);
        var tripletdB = getNbw_µO_µo(spk2stats[spk].obsShape2decdB);
        var tripletdur = getNbw_µO_µo(spk2stats[spk].obsShape2decdur);

        spk2stats[spk].obsShape2dec = [triplet[1],triplet[2]]
        spk2stats[spk].diffdec = triplet[2] - triplet[1]
        spk2stats[spk].obsShape2decF0 = [tripletF0[1],tripletF0[2]]
        spk2stats[spk].diffdecF0 = tripletF0[2] - tripletF0[1]
        spk2stats[spk].obsShape2decdB = [tripletdB[1],tripletdB[2]]
        spk2stats[spk].diffdecdB = tripletdB[2] - tripletdB[1]
        spk2stats[spk].obsShape2decdur = [tripletdur[1],tripletdur[2]]
        spk2stats[spk].diffdecdur = tripletdur[2] - tripletdur[1]

        spkratediff.push([spk, spk2stats[spk].rateExpIsObs, spk2stats[spk].diffdec, spk2stats[spk].diffdecF0, spk2stats[spk].diffdecdB, spk2stats[spk].diffdecdur] )
    }

    function getNbw_µO_µo(shape2decs) {
        var triplets = []; // list of [nbWords,decileStressed,decileUnstressed]
        for (obsShape in shape2decs) {
            var xO = -1
            var sums = [] // liste des sommes de syllabe
            for (x in obsShape) { 
                if (obsShape[x] == "O") { xO=x }
                sums.push(0)
            }
            
            var cptw = 0;
            for (strdecs of shape2decs[obsShape]) {
                var listeDecs = string2list(strdecs)
                for (dec in listeDecs) {
                    sums[dec] += listeDecs[dec]
                }
                cptw++;
            }

            var means = [];
            for (let sum in sums) {
                means.push(sums[sum]/cptw)
            }

            triplets.push( [ cptw, means.splice(xO,1)[0], mean(means) ] )
        }

        var sumsWithCoef = [0,0,0];
        triplets.forEach((t)=>{
            sumsWithCoef[0] += t[0]
            sumsWithCoef[1] += t[0]*t[1]
            sumsWithCoef[2] += t[0]*t[2]
        })
        var stressed = Number( sumsWithCoef[1]/sumsWithCoef[0] ).toFixed()
        var unstressed = Number( sumsWithCoef[2]/sumsWithCoef[0] ).toFixed()

        return [sumsWithCoef[0], unstressed, stressed]
    }

    console.table(spkratediff)
    return spk2stats
}


function getStatPerSpkExp() {
    // Filtrer depuis l'interface en amont;
    // Cette fonction loop sur datafiltre
    // PER SPEAKER:
    //      Compute rate of words correctly shaped
    //      Compute multidimensional (o/O)
    //      Compute mean Pitch (o/O)
    //      Compute mean Energy (o/O)
    //      Compute mean Duration (o/O)
    //
    //   EXPECTED SHAPE

    var spk2stats = {};
    spkratediff = [];

    datafiltre.forEach((el)=>{
        if (!Object.keys(spk2stats).includes(el.spk)) {
            spk2stats[el.spk] = {
                spkId : el.spk,
                clesIo: locuteurs[el.spk].CLES_IO,
                nbExpIsObs : 0,     // nb of words correctly shaped
                nbW : 0,            // nb of words
                rateExpIsObs : 0,   // rate of words correctly shaped
                expShape2dec: {},   // Multidimensional (o/O)
                expShape2decF0: {}, // Mean Pitch (o/O)
                expShape2decdB: {}, // Mean Energy (o/O)
                expShape2decdur: {},// Mean Duration (o/O)
                diffdec: 0,         // difference O-o
                diffdecF0: 0,       // difference O-o F0
                diffdecdB: 0,       // difference O-o dB
                diffdecdur: 0,      // difference O-o dur
            }
        }

        spk2stats[el.spk].nbW++;
        if (el.expectedShape==el.observedShape) spk2stats[el.spk].nbExpIsObs++;

        if (!Object.keys(spk2stats[el.spk].expShape2dec).includes(el.expectedShape)) {
            spk2stats[el.spk].expShape2dec[el.expectedShape] = []
            spk2stats[el.spk].expShape2decF0[el.expectedShape] = []
            spk2stats[el.spk].expShape2decdB[el.expectedShape] = []
            spk2stats[el.spk].expShape2decdur[el.expectedShape] = []
        }
        spk2stats[el.spk].expShape2dec[el.expectedShape].push(el.globalDeciles)
        spk2stats[el.spk].expShape2decF0[el.expectedShape].push(el.F0Deciles)
        spk2stats[el.spk].expShape2decdB[el.expectedShape].push(el.dBDeciles)
        spk2stats[el.spk].expShape2decdur[el.expectedShape].push(el.durDeciles)
    })

    for (spk in spk2stats) {
        spk2stats[spk].rateExpIsObs = (spk2stats[spk].nbExpIsObs*100/spk2stats[spk].nbW).toFixed(2)

        var triplet = getNbw_µO_µo(spk2stats[spk].expShape2dec);
        var tripletF0 = getNbw_µO_µo(spk2stats[spk].expShape2decF0);
        var tripletdB = getNbw_µO_µo(spk2stats[spk].expShape2decdB);
        var tripletdur = getNbw_µO_µo(spk2stats[spk].expShape2decdur);

        spk2stats[spk].expShape2dec = [triplet[1],triplet[2]]
        spk2stats[spk].diffdec = triplet[2] - triplet[1]
        spk2stats[spk].expShape2decF0 = [tripletF0[1],tripletF0[2]]
        spk2stats[spk].diffdecF0 = tripletF0[2] - tripletF0[1]
        spk2stats[spk].expShape2decdB = [tripletdB[1],tripletdB[2]]
        spk2stats[spk].diffdecdB = tripletdB[2] - tripletdB[1]
        spk2stats[spk].expShape2decdur = [tripletdur[1],tripletdur[2]]
        spk2stats[spk].diffdecdur = tripletdur[2] - tripletdur[1]

        spkratediff.push([spk, spk2stats[spk].rateExpIsObs, spk2stats[spk].diffdec, spk2stats[spk].diffdecF0, spk2stats[spk].diffdecdB, spk2stats[spk].diffdecdur] )
    }

    function getNbw_µO_µo(shape2decs) {
        var triplets = []; // list of [nbWords,decileStressed,decileUnstressed]
        for (expShape in shape2decs) {
            var xO = -1
            var sums = [] // liste des sommes de syllabe
            for (x in expShape) { 
                if (expShape[x] == "O") { xO=x }
                sums.push(0)
            }
            
            var cptw = 0;
            for (strdecs of shape2decs[expShape]) {
                var listeDecs = string2list(strdecs)
                for (dec in listeDecs) {
                    sums[dec] += listeDecs[dec]
                }
                cptw++;
            }

            var means = [];
            for (let sum in sums) {
                means.push(sums[sum]/cptw)
            }

            triplets.push( [ cptw, means.splice(xO,1)[0], mean(means) ] ) // splice extrait la syllabe accentuée (en position xO) et laisse seulement les non accentuées dans means.
        }

        var sumsWithCoef = [0,0,0];
        triplets.forEach((t)=>{
            sumsWithCoef[0] += t[0]
            sumsWithCoef[1] += t[0]*t[1]
            sumsWithCoef[2] += t[0]*t[2]
        })
        var stressed = Number( sumsWithCoef[1]/sumsWithCoef[0] ).toFixed()
        var unstressed = Number( sumsWithCoef[2]/sumsWithCoef[0] ).toFixed()

        return [sumsWithCoef[0], unstressed, stressed]
    }

    console.table(spkratediff)
    return spk2stats
}

// bibibi(getStatPerSpk()) 
function bibibi(spk2stats) {
    var div3d = document.getElementById('plot3d');
    div3d.style.width= "80%";
    div3d.style.height= "800px";

    function unpack(dic,col,cond){
        return Object.keys(dic).map(function(key) { 
            if (col=="") { return key }
            else if (dic[key].clesIo == cond) { return dic[key][col]; }
        });
    }    

    var trace1 = {
        name: "CLES IO B1",
        x: unpack(spk2stats, 'diffdecdur', 'B1'), 
        y: unpack(spk2stats, 'diffdecF0', 'B1'), 
        z: unpack(spk2stats, 'diffdecdB', 'B1'),
        text: unpack(spk2stats, '', 'B1'),
        mode: 'markers',
        marker: {
            size: 5,
            line: {
                color: 'rgba(217, 217, 217, 0.14)',
                width: 0.5
            },
            opacity: 0.8
        },
        type: 'scatter3d'
    };

    var trace2 = {
        name: "CLES IO B2",
        x: unpack(spk2stats, 'diffdecdur', 'B2'), 
        y: unpack(spk2stats, 'diffdecF0', 'B2'), 
        z: unpack(spk2stats, 'diffdecdB', 'B2'),
        text: unpack(spk2stats, '', 'B2'),
        mode: 'markers',
        marker: {
            size: 5,
            line: {
                color: 'rgba(100, 199, 217, 0.14)',
                width: 0.5
            },
            opacity: 0.8
        },
        type: 'scatter3d'
    };

    var data = [trace1, trace2];


    var layout = {
        title: { 
            text: "Speaker stress quality",
            font: {
                family: 'Courier New, monospace',
                size: 24
            },
            xref: 'paper',
            x: 0.05
        },
        scene: {
            xaxis:{
                title: 'Duration (x)',
                backgroundcolor: "rgb(116,102,135)",
                gridcolor: "rgb(255, 255, 255)",
                showbackground: true,
                zerolinecolor: "rgb(255, 255, 255)",
            },
            yaxis:{
                title: 'Pitch (y)',
                backgroundcolor: "rgb(179,171,196)",
                gridcolor: "rgb(255, 255, 255)",
                showbackground: true,
                zerolinecolor: "rgb(255, 255, 255)",
            },
            zaxis:{
                title: 'Energy (z)',
                backgroundcolor: "rgb(169,189,202)",
                gridcolor: "rgb(255, 255, 255)",
                showbackground: true,
                zerolinecolor: "rgb(255, 255, 255)",
            },
        },
    };
    Plotly.newPlot(div3d, data, layout);



    var div2d = document.getElementById('plotRateGlob');
    div2d.style.width= "80%";
    div2d.style.height= "800px";

    var trace12 = {
        name: "CLES IO B1",
        x: unpack(spk2stats, 'rateExpIsObs', 'B1'), 
        y: unpack(spk2stats, 'diffdec', 'B1'), 
        text: unpack(spk2stats, '', 'B1'),
        mode: 'markers',
        marker: {
            size: 20,
            line: {
                color: 'rgba(217, 217, 217, 0.14)',
                width: 0.5
            },
            opacity: 0.8
        },
        type: 'scatter'
    };

    var trace22 = {
        name: "CLES IO B2",
        x: unpack(spk2stats, 'rateExpIsObs', 'B2'), 
        y: unpack(spk2stats, 'diffdec', 'B2'), 
        text: unpack(spk2stats, '', 'B2'),
        mode: 'markers',
        marker: {
            size: 20,
            line: {
                color: 'rgba(100, 199, 217, 0.14)',
                width: 0.5
            },
            opacity: 0.8
        },
        type: 'scatter'
    };

    var data2 = [trace12, trace22];


    var layout2 = {
        title: { 
            text: "Proportion of expected-is-observed given multidimensional o/O",
            font: {
                family: 'Courier New, monospace',
                size: 24
            },
            xref: 'paper',
            x: 0.05
        },
        xaxis: {
            title: {
              text: 'Proportion of words with expected prosodic shape',
              font: {
                family: 'Courier New, monospace',
                size: 15,
                color: '#7f7f7f'
              }
            },
          },
        yaxis: {
            title: {
              text: 'Difference between stressed and unstressed syllable (multidimensional)',
              font: {
                family: 'Courier New, monospace',
                size: 15,
                color: '#7f7f7f'
              }
            },
        },
    };
    Plotly.newPlot(div2d, data2, layout2);
}
