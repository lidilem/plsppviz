# Visualisation interface for PLSPP pipeline

This repo contains a Django server-based interface allowing visualisation of lexical stress output from [plspp pipeline](https://gricad-gitlab.univ-grenoble-alpes.fr/lidilem/plspp). This pipeline also comes with a html/js-only stand alone visualisation tool, check it out if you prefer a simple tool your visualisations ;)

The interface needs the following outputs from plspp: `speakers.csv`, `stressTable.csv`, `wordTable.csv` to display the lexical stress visualisation; as well as `pauseTable.csv` for displaying the pause pattern analysis. You can optionnally put audio files in `static/audio/` to play words when clicking on them. Change the paths in `app/views.py`. The interface allows access restriction with user accounts as well.

Here is an example of a currently hosted version of the interface: [https://plspp.univ-grenoble-alpes.fr/](https://plspp.univ-grenoble-alpes.fr/)

# Installation

```shell
# Install Django 4 and needed dependencies
pip install -r requirements.txt

# Run the server
./manage.py runserver
```

More documentation to come. [Contact me](mailto:sylvain.coulange@univ-grenoble-alpes.fr) if you need some quickly :)

# Examples of visualisations

![Overview of plsppViz](static/plsppViz_overview.png)
Examples of stress global analysis (left), lexical stress analysis (mid.), pause pattern analysis (right).