from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect


# TUTO : https://www.youtube.com/watch?v=eBsc65jTKvw

def unauthenticated_user(view_func):
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('home')
        else:
            return view_func(request, *args, **kwargs)

    return wrapper_func

def allowed_users(allowed_roles=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):

            groups = []
            if request.user.groups.exists():
                groups = [x.name for x in request.user.groups.all()]
                print("Current user groups:",groups)
                print("Allowed groups are:",allowed_roles)
            
            for group in groups:
                if group in allowed_roles:
                    return view_func(request, *args, **kwargs)

            return HttpResponseRedirect('/accessDenied')
            
        return wrapper_func
    return decorator