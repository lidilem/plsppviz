from django.shortcuts import render, redirect, HttpResponseRedirect
from django.http import JsonResponse
from stressViz import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required

from .forms import UserLoginForm
from .decorators import unauthenticated_user, allowed_users
import os, json, re

def home(request):
    return render(request, 'stress_vizLocalFiles.html', { "dataset":"local" })


@unauthenticated_user
def loginView(request):
    nextPage = request.GET.get('next')
    print("NextPage=",nextPage)
    form = UserLoginForm(request.POST or None)

    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username = username, password = password)
        
        if user is not None:
            login(request, user)

            if nextPage:
                return redirect(nextPage)
            return redirect('/')
    
    context = {
        'form': form,
        'dataset': "none"
    }
    return render(request, 'login.html', context)


def accessDenied(request):
    return render(request, 'accessDenied.html', { "dataset":"none" })


# PROJET CLES
@login_required
@allowed_users(allowed_roles=['cles'])
def cles(request):
    rawCSVlocuteurs = []
    with open(os.path.join(settings.BASE_DIR,"static/data/CLES/speakers.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVlocuteurs.append(line)
    print("Len locuteurs file:", len(rawCSVlocuteurs))

    rawCSVstress = []
    with open(os.path.join(settings.BASE_DIR,"static/data/CLES/stressTable.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVstress.append(line)
    print("Len stress file:", len(rawCSVstress))

    audioFolder = "/static/audio/CLES"

    context = { 
        "rawCSVlocuteurs": ''.join(rawCSVlocuteurs), 
        "rawCSVstress": ''.join(rawCSVstress), 
        "audioFolder":audioFolder, 
        "dataset":"CLES",
        "stress": True,
        "pauses": True,
        'audioExt':getAudioExt('CLES') }

    return render(request, 'stress_viz.html', context)



# PROJET SUGAHARA
@login_required
@allowed_users(allowed_roles=['sugahara'])
def sugahara(request):
    rawCSVlocuteurs = []
    with open(os.path.join(settings.BASE_DIR,"static/data/sugahara/speakers.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVlocuteurs.append(line)
    print("Len locuteurs file:", len(rawCSVlocuteurs))

    rawCSVstress = []
    with open(os.path.join(settings.BASE_DIR,"static/data/sugahara/stressTable.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVstress.append(line)
    print("Len stress file:", len(rawCSVstress))

    audioFolder = "/static/audio/sugahara"

    context = { 
        "rawCSVlocuteurs": ''.join(rawCSVlocuteurs), 
        "rawCSVstress": ''.join(rawCSVstress), 
        "audioFolder":audioFolder, 
        "dataset":"sugahara", 
        "stress": True,
        "pauses": False,
        'audioExt':getAudioExt('sugahara')  
    }

    return render(request, 'stress_viz.html', context)


# PROJET PIC
def mallory(request):
    rawCSVlocuteurs = []
    with open(os.path.join(settings.BASE_DIR,"static/data/mallory/speakers.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVlocuteurs.append(line)
    print("Len locuteurs file:", len(rawCSVlocuteurs))

    rawCSVstress = []
    with open(os.path.join(settings.BASE_DIR,"static/data/mallory/stressTable.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVstress.append(line)
    print("Len stress file:", len(rawCSVstress))

    audioFolder = "/static/audio/mallory"

    context = { 
        "rawCSVlocuteurs": ''.join(rawCSVlocuteurs), 
        "rawCSVstress": ''.join(rawCSVstress), 
        "audioFolder": audioFolder, 
        "dataset":"mallory", 
        "stress": True,
        "pauses": False,
        'audioExt':getAudioExt('mallory')  
    }

    return render(request, 'stress_viz.html', context)

# PROJET CLES-JP (Waseda, Doshisha; Konishi sensei)
def konishi(request):
    rawCSVlocuteurs = []
    with open(os.path.join(settings.BASE_DIR,"static/data/konishi/speakers.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVlocuteurs.append(line)
    print("Len locuteurs file:", len(rawCSVlocuteurs))

    rawCSVstress = []
    with open(os.path.join(settings.BASE_DIR,"static/data/konishi/stressTable.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVstress.append(line)
    print("Len stress file:", len(rawCSVstress))

    audioFolder = "/static/audio/konishi"

    context = { 
        "rawCSVlocuteurs": ''.join(rawCSVlocuteurs), 
        "rawCSVstress": ''.join(rawCSVstress), 
        "audioFolder":audioFolder, 
        "dataset":"konishi", 
        "stress": True,
        "pauses": True,
        'audioExt':getAudioExt('konishi')  
    }

    return render(request, 'stress_viz.html', context)

# CORPUS ICNALE (Kobe Univ.)
def icnale(request):
    rawCSVlocuteurs = []
    with open(os.path.join(settings.BASE_DIR,"static/data/ICNALE/speakers.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVlocuteurs.append(line)
    print("Len locuteurs file:", len(rawCSVlocuteurs))

    rawCSVstress = []
    with open(os.path.join(settings.BASE_DIR,"static/data/ICNALE/stressTable.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVstress.append(line)
    print("Len stress file:", len(rawCSVstress))

    audioFolder = "/static/audio/ICNALE"

    context = { 
        "rawCSVlocuteurs": ''.join(rawCSVlocuteurs), 
        "rawCSVstress": ''.join(rawCSVstress), 
        "audioFolder":audioFolder, 
        "dataset":"ICNALE", 
        "stress": True,
        "pauses": True,
        'audioExt':getAudioExt('ICNALE')  
    }

    return render(request, 'stress_viz.html', context)


# CORPUS Nakanishi (Kobe Gakuin Univ.)
# @login_required
# @allowed_users(allowed_roles=['nakanishi'])
def nakanishi(request):
    return getStressGlobalView(request, dataset="nakanishi", pauses=False)

# @login_required
# @allowed_users(allowed_roles=['nakanishi'])
def nakanishiTest(request):
    return getStressGlobalView(request, dataset="nakanishiTest", pauses=False)

# CLES-DOSHISHA
def doshisha(request):
    return getStressGlobalView(request, dataset="doshisha", pauses=True)

# CLES-JP
def clesjp(request):
    return getStressGlobalView(request, dataset="CLESJP", pauses=True)

# ERICKSON
@login_required
@allowed_users(allowed_roles=['erickson'])
def erickson(request):
    return getStressGlobalView(request, dataset="erickson", pauses=False)

# MAY WU
@login_required
@allowed_users(allowed_roles=['wu'])
def wu1(request):
    return getStressGlobalView(request, dataset="wu1", pauses=True)

@login_required
@allowed_users(allowed_roles=['wu'])
def wu2(request):
    return getStressGlobalView(request, dataset="wu2", pauses=False)

@login_required
@allowed_users(allowed_roles=['wu'])
def wu3(request):
    return getStressGlobalView(request, dataset="wu3", pauses=False)

# MAREKOVA BENUS
@login_required
@allowed_users(allowed_roles=['marekova'])
def marekova(request):
    return getStressGlobalView(request, dataset="marekova", pauses=True)


def getStressGlobalView(request, dataset="CLES", stress=True, pauses=True):
    rawCSVlocuteurs = []
    with open(os.path.join(settings.BASE_DIR,"static/data/"+dataset+"/speakers.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVlocuteurs.append(line)
    print("Len locuteurs file:", len(rawCSVlocuteurs))

    rawCSVstress = []
    with open(os.path.join(settings.BASE_DIR,"static/data/"+dataset+"/stressTable.csv"),"r") as inf: 
        lines = inf.readlines()
        for line in lines:
            rawCSVstress.append(line)
    print("Len stress file:", len(rawCSVstress))

    audioFolder = "/static/audio/"+dataset
    
    context = { 
        "rawCSVlocuteurs": ''.join(rawCSVlocuteurs), 
        "rawCSVstress": ''.join(rawCSVstress), 
        "audioFolder":audioFolder, 
        "dataset":dataset, 
        "stress": stress,
        "pauses": pauses,
        'audioExt':getAudioExt(dataset)  }

    return render(request, 'stress_viz.html', context)



# PAUSES VIZ
@login_required
@allowed_users(allowed_roles=['cles'])
def pausesvizCLES(request):
    return render(request, 'pause_viz.html', { "dataset":"CLES", "stress":True, "pauses":True, 'audioExt':getAudioExt('CLES') })

@login_required
@allowed_users(allowed_roles=['sugahara'])
def pausesvizSugahara(request):
    return render(request, 'pause_viz.html', { "dataset":"sugahara", "stress":True, "pauses":False, 'audioExt':getAudioExt('sugahara') })

def pausesvizMallory(request):
    return render(request, 'pause_viz.html', { "dataset":"mallory", "stress":True, "pauses":False, 'audioExt':getAudioExt('mallory') })

def pausesvizKonishi(request):
    return render(request, 'pause_viz.html', { "dataset":"konishi", "stress":True, "pauses":True, 'audioExt':getAudioExt('konishi') })

def pausesvizICNALE(request):
    return render(request, 'pause_viz.html', { "dataset":"ICNALE", "stress":True, "pauses":True, 'audioExt':getAudioExt('ICNALE') })

# @login_required
# @allowed_users(allowed_roles=['nakanishi'])
def pausesvizNakanishi(request):
    return render(request, 'pause_viz.html', { "dataset":"nakanishi", "stress":True, "pauses":True, 'audioExt':getAudioExt('nakanishi') })

def pausesvizDoshisha(request):
    return render(request, 'pause_viz.html', { "dataset":"doshisha", "stress":True, "pauses":True, 'audioExt':getAudioExt('doshisha') })

def pausesvizCLESJP(request):
    return render(request, 'pause_viz.html', { "dataset":"CLESJP", "stress":True, "pauses":True, 'audioExt':getAudioExt('CLESJP') })

@login_required
@allowed_users(allowed_roles=['erickson'])
def pausesvizErickson(request):
    return render(request, 'pause_viz.html', { "dataset":"erickson", "stress":True, "pauses":False, 'audioExt':getAudioExt('erickson') })

@login_required
@allowed_users(allowed_roles=['wu'])
def pausesvizWu1(request):
    return render(request, 'pause_viz.html', { "dataset":"wu1", "stress":True, "pauses":False, 'audioExt':getAudioExt('wu1') })

@login_required
@allowed_users(allowed_roles=['marekova'])
def pausesvizMarekova(request):
    return render(request, 'pause_viz.html', { "dataset":"marekova", "stress":True, "pauses":True, 'audioExt':getAudioExt('marekova') })


def getSpeakers(request):
    colis = json.loads(request.body)
    print(colis)
    dataset = colis['dataset']
    speakers = {}
    header = []
    with open(os.path.join(settings.BASE_DIR,"static/data/"+dataset+"/speakers.csv"),"r") as inf: 
        for i, line in enumerate(inf):
            line = line.strip()
            l = line.split(";")

            if i==0:
                for h in l:
                    header.append(h)
            else:
                newSpeaker = {}
                for i2, c in enumerate(l):
                    newSpeaker[header[i2]] = c
                speakers[l[0]] = newSpeaker

    print("Dataset:", dataset, "| Len locuteurs file:", len(speakers.keys()))

    return JsonResponse(speakers)



def getSpeakerSegments(request):
    colis = json.loads(request.body)
    targetSpeaker = colis['speaker']
    # targetSpeaker = "mai2022-103_021-022_SPEAKER_00"
    dataset = colis['dataset']
    minNbTokens = colis['minNbTokens']

    segments = []
    header = []
    currentSegment = []
    previousWordFilename = ""
    with open(os.path.join(settings.BASE_DIR,"static/data/"+dataset+"/pauseTable.csv"),"r") as inf: 
        for i, line in enumerate(inf):
            line = line.strip()
            l = line.split(";")

            if i==0:
                for h in l:
                    header.append(h)
            else:
                if l[0]==targetSpeaker or l[1]==targetSpeaker:
                    currentFilename = l[1]
                    currentWord = {}
                    for i2, c in enumerate(l):
                        currentWord[header[i2]] = c
                    
                    if currentFilename!=previousWordFilename or previousWordFilename=="":
                        segments.append(currentSegment)
                        currentSegment = []
        
                    currentSegment.append(currentWord)
                    previousWordFilename = currentFilename
        
        segments.append(currentSegment)

        

    print("Dataset:", dataset, "| Number of segments for", targetSpeaker, " (min nb tokens=",minNbTokens,"):", len(segments))

    return JsonResponse({"segments":segments})


def makeSegmentsStats(request):
    ## This function parse pauseTable.csv and list topX segments with specific traits (e.g. max nb of intra-phrase pauses)
    colis = json.loads(request.body)
    pauseThreshold = colis["pauseThreshold"]
    speakersB1B2 = colis["speakersB1B2"]
    dataset = colis['dataset']
    minNbTokens = colis['minNbTokens']

    topX = 15 # nb of entries to keep

    statsSegments = {
        "nbTokens": [],
        "nbPauses": [],
        "nbPausesInterClause": [],
        "nbPausesIntraPhrase": [],
        "totalDurationPauses": [],
        "rationbPauses_nbTokens": [],
        "rationbPausesInterClause_nbPauses": [],
        "rationbPausesIntraPhrase_nbPauses": [],
    }

    segs2stats = {} # for each segment, list stats (nb of pauses, nb of intra-phrase pauses...)
    
    header = []

    clauseTag = ["S","SBAR","SBARQ","SINV","SQ"]
    phraseTag = ["ADJP","ADVP","CONJP","FRAG","INTJ","LST","NAC","NP","NX","PP","PRN","PRT","QP","RRC","UCP","VP","WHADJP","WHAVP","WHNP","WHPP","X"]
    conjTag = ["CC","IN"]

    # First parse to get the list of segments to analyze (skip those under minNbTokens)
    listOfSegmentsToAnalyze = []
    seg2length = {}
    with open(os.path.join(settings.BASE_DIR,"static/data/"+dataset+"/pauseTable.csv"),"r") as inf: 
        inf.readline()
        for i, line in enumerate(inf):
            line = line.strip()
            l = line.split(";")
            if l[1] not in seg2length.keys():
                seg2length[l[1]] = 0
            seg2length[l[1]] += 1
    for seg, length in seg2length.items():
        if length >= minNbTokens:
            listOfSegmentsToAnalyze.append(seg)

    # Then, parse again to do the stats
    with open(os.path.join(settings.BASE_DIR,"static/data/"+dataset+"/pauseTable.csv"),"r") as inf: 
        for i, line in enumerate(inf):
            line = line.strip()
            l = line.split(";")
            spk, file, ix, POScontextLeft, POScontextRight, duration, wordLeft, wordLeftEndingLarger, wordLeftEndingLargerNb, wordLeftDepth, wordLeftTagw, wordRight, wordRightStartingLarger, wordRightStartingLargerNb, wordRightDepth, wordRightTagw, deb, fin = l
            if i==0:
                for h in l:
                    header.append(h)
            elif file in listOfSegmentsToAnalyze:
                if spk in speakersB1B2:
                    if file not in segs2stats.keys():
                        segs2stats[file] = {
                            "nbTokens":0,
                            "nbPauses":0,
                            "nbPausesInterClause":0,
                            "nbPausesIntraPhrase":0,
                            "totalDurationPauses":0,
                        }
                    segs2stats[file]['nbTokens'] += 1
                    duration = float(duration)

                    if duration > pauseThreshold['min'] and duration <= pauseThreshold['max']:
                        # This is a pause
                        segs2stats[file]['nbPauses'] += 1
                        segs2stats[file]['totalDurationPauses'] += duration

                        if wordLeftEndingLarger in clauseTag or wordRightStartingLarger in clauseTag:
                            # This is an inter-clause pause
                            segs2stats[file]['nbPausesInterClause'] += 1
                        elif wordLeftEndingLarger in phraseTag or wordRightStartingLarger in phraseTag:
                            # This is an inter-phrase pause
                            # do nothing
                            continue
                        else:
                            segs2stats[file]['nbPausesIntraPhrase'] += 1

    # Make some other stats on each segment
    for seg, stats in segs2stats.items():
        segs2stats[seg]["rationbPauses_nbTokens"] = stats['nbPauses'] / stats['nbTokens']
        segs2stats[seg]["rationbPausesInterClause_nbPauses"] = 0
        segs2stats[seg]["rationbPausesIntraPhrase_nbPauses"] = 0
        if stats['nbPauses'] > 0:
            segs2stats[seg]["rationbPausesInterClause_nbPauses"] = stats['nbPausesInterClause'] / stats['nbPauses']
            segs2stats[seg]["rationbPausesIntraPhrase_nbPauses"] = stats['nbPausesIntraPhrase'] / stats['nbPauses']


    # Make TopX lists for each parameter
    for s in statsSegments.keys():
        cpt = 0
        print(s)
        for seg, stats in sorted(segs2stats.items(), key=lambda t:t[1][s], reverse=True):
            if cpt <= topX:
                statsSegments[s].append(seg)
                print(seg, stats[s])
                cpt+=1

    return JsonResponse(statsSegments)


def getTopSegments(request):
    colis = json.loads(request.body)
    
    segList = colis['segList']
    dataset = colis['dataset']

    segments = []
    header = []
    currentSegment = []
    previousWordFilename = ""

    with open(os.path.join(settings.BASE_DIR,"static/data/"+dataset+"/pauseTable.csv"),"r") as inf: 
        for i, line in enumerate(inf):
            line = line.strip()
            l = line.split(";")

            if i==0:
                for h in l:
                    header.append(h)
            else:
                if l[1] in segList:
                    currentFilename = l[1]
                    currentWord = {}
                    for i2, c in enumerate(l):
                        currentWord[header[i2]] = c
                    
                    if currentFilename!=previousWordFilename and previousWordFilename!="":
                        segments.append(currentSegment)
                        currentSegment = []
        
                    currentSegment.append(currentWord)
                    previousWordFilename = currentFilename

    print("Segments found:", len(segments))

    # Reorder segments
    orderedSegments = []
    for s in segList:
        for x in segments:
            if x[0]['file'] == s:
                orderedSegments.append(x)

    print("Sending back wanted segments:", len(orderedSegments))

    return JsonResponse({"segments":orderedSegments})


# STRESS DETAILED VIZ
def segments(request, dataset):
    return render(request, 'segments.html', {'dataset':dataset})

@login_required
@allowed_users(allowed_roles=['cles'])
def segmentsCLES(request):
    return render(request, 'segments.html', {'dataset':'CLES', "stress":True, "pauses":True, 'audioExt':getAudioExt('CLES')})


@login_required
@allowed_users(allowed_roles=['sugahara'])
def segmentsSugahara(request):
    return render(request, 'segments.html', {'dataset':'sugahara', "stress":True, "pauses":False, 'audioExt':getAudioExt('sugahara') })

def segmentsMallory(request):
    return render(request, 'segments.html', {'dataset':'mallory', "stress":True, "pauses":False, 'audioExt':getAudioExt('mallory') })

def segmentsKonishi(request):
    return render(request, 'segments.html', {'dataset':'konishi', "stress":True, "pauses":True, 'audioExt':getAudioExt('konishi') })

def segmentsICNALE(request):
    return render(request, 'segments.html', {'dataset':'ICNALE', "stress":True, "pauses":True, 'audioExt':getAudioExt('ICNALE') })

# @login_required
# @allowed_users(allowed_roles=['nakanishi'])
def segmentsNakanishi(request):
    return render(request, 'segments.html', {'dataset':'nakanishi', "stress":True, "pauses":True, 'audioExt':getAudioExt('nakanishi') })

# @login_required
# @allowed_users(allowed_roles=['nakanishi'])
def segmentsNakanishiTest(request):
    return render(request, 'segments.html', {'dataset':'nakanishiTest', "stress":True, "pauses":True, 'audioExt':getAudioExt('nakanishi') })

def segmentsDoshisha(request):
    return render(request, 'segments.html', {'dataset':'doshisha', "stress":True, "pauses":True, 'audioExt':getAudioExt('doshisha') })

def segmentsCLESJP(request):
    return render(request, 'segments.html', {'dataset':'CLESJP', "stress":True, "pauses":True, 'audioExt':getAudioExt('CLESJP') })

@login_required
@allowed_users(allowed_roles=['erickson'])
def segmentsErickson(request):
    return render(request, 'segments.html', {'dataset':'erickson', "stress":True, "pauses":False, 'audioExt':getAudioExt('erickson') })

@login_required
@allowed_users(allowed_roles=['wu'])
def segmentsWu1(request):
    return render(request, 'segments.html', {'dataset':'wu1', "stress":True, "pauses":True, 'audioExt':getAudioExt('wu1') })

@login_required
@allowed_users(allowed_roles=['wu'])
def segmentsWu2(request):
    return render(request, 'segments.html', {'dataset':'wu2', "stress":True, "pauses":False, 'audioExt':getAudioExt('wu2') })

@login_required
@allowed_users(allowed_roles=['wu'])
def segmentsWu3(request):
    return render(request, 'segments.html', {'dataset':'wu3', "stress":True, "pauses":False, 'audioExt':getAudioExt('wu3') })

@login_required
@allowed_users(allowed_roles=['marekova'])
def segmentsMarekova(request):
    return render(request, 'segments.html', {'dataset':'marekova', "stress":True, "pauses":True, 'audioExt':getAudioExt('marekova') })


def getWordSegments(request):
    colis = json.loads(request.body)
    targetSpeaker = colis['speaker']
    # targetSpeaker = "mai2022-103_021-022_SPEAKER_00"
    dataset = colis['dataset']

    segments = []
    header = []
    currentSegment = []
    previousWordFilename = ""
    with open(os.path.join(settings.BASE_DIR,"static/data/"+dataset+"/wordTable.csv"),"r") as inf: 
        for i, line in enumerate(inf):
            line = line.strip()
            l = line.split(";")

            if i==0:
                for h in l:
                    header.append(h)
            else:
                if l[1]==targetSpeaker or l[0]==targetSpeaker:
                    currentFilename = l[0]
                    currentWord = {}
                    for i2, c in enumerate(l):
                        currentWord[header[i2]] = c
                    
                    if currentFilename!=previousWordFilename or previousWordFilename=="":
                        segments.append(currentSegment)
                        currentSegment = []
        
                    currentSegment.append(currentWord)
                    previousWordFilename = currentFilename
        if len(currentSegment)!=0:
            segments.append(currentSegment)

    print("Dataset:",dataset, "| Number of segments for", targetSpeaker, ":", len(segments))

    return JsonResponse({"segments":segments})


def getAudioExt(dataset):
    firstFile = os.listdir(os.path.join(settings.BASE_DIR,"static/audio/"+dataset+"/"))[0]
    ext = '.'+'.'.join(firstFile.split('.')[1:])
    print(firstFile, ext)
    return ext
