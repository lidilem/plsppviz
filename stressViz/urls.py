"""
URL configuration for stressViz project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views
from app import views as app_views
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', csrf_exempt(app_views.loginView), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name="users/logout.html"), name='logout'),
    path('', app_views.home, name="home"),
    path('accessDenied/', app_views.accessDenied, name="accessDenied"),
    
    path('CLES/', app_views.cles, name="cles"),
    path('mallory/', app_views.mallory, name="mallory"),
    path('sugahara/', app_views.sugahara, name="sugahara"),
    path('konishi/', app_views.konishi, name="konishi"),
    path('ICNALE/', app_views.icnale, name="icnale"),
    path('nakanishi/', app_views.nakanishi, name="nakanishi"),
    path('nakanishiTest/', app_views.nakanishiTest, name="nakanishiTest"),
    path('doshisha/', app_views.doshisha, name="doshisha"),
    path('CLESJP/', app_views.clesjp, name="CLESJP"),
    path('erickson/', app_views.erickson, name="erickson"),
    path('wu1/', app_views.wu1, name="wu1"),
    path('wu2/', app_views.wu2, name="wu2"),
    path('wu3/', app_views.wu3, name="wu3"),
    path('marekova/', app_views.marekova, name="marekova"),

    path('pausesviz/CLES', app_views.pausesvizCLES, name="pausesvizCLES"),
    path('pausesviz/mallory', app_views.pausesvizMallory, name="pausesvizMallory"),
    path('pausesviz/sugahara', app_views.pausesvizSugahara, name="pausesvizSugahara"),
    path('pausesviz/konishi', app_views.pausesvizKonishi, name="pausesvizKonishi"),
    path('pausesviz/ICNALE', app_views.pausesvizICNALE, name="pausesvizICNALE"),
    path('pausesviz/nakanishi', app_views.pausesvizNakanishi, name="pausesvizNakanishi"),
    path('pausesviz/doshisha', app_views.pausesvizDoshisha, name="pausesvizDoshisha"),
    path('pausesviz/CLESJP', app_views.pausesvizCLESJP, name="pausesvizCLESJP"),
    path('pausesviz/erickson', app_views.pausesvizErickson, name="pausesvizErickson"),
    path('pausesviz/wu1', app_views.pausesvizWu1, name="pausesvizWu1"),
    path('pausesviz/marekova', app_views.pausesvizMarekova, name="pausesvizMarekova"),
    
    path('segments/CLES', app_views.segmentsCLES, name="segmentsCLES"),
    path('segments/mallory', app_views.segmentsMallory, name="segmentsMallory"),
    path('segments/sugahara', app_views.segmentsSugahara, name="segmentsSugahara"),
    path('segments/konishi', app_views.segmentsKonishi, name="segmentsKonishi"),
    path('segments/ICNALE', app_views.segmentsICNALE, name="segmentsICNALE"),
    path('segments/nakanishi', app_views.segmentsNakanishi, name="segmentsNakanishi"),
    path('segments/nakanishiTest', app_views.segmentsNakanishiTest, name="segmentsNakanishiTest"),
    path('segments/doshisha', app_views.segmentsDoshisha, name="segmentsDoshisha"),
    path('segments/CLESJP', app_views.segmentsCLESJP, name="segmentsCLESJP"),
    path('segments/erickson', app_views.segmentsErickson, name="segmentsErickson"),
    path('segments/wu1', app_views.segmentsWu1, name="segmentsWu1"),
    path('segments/wu2', app_views.segmentsWu2, name="segmentsWu2"),
    path('segments/wu3', app_views.segmentsWu3, name="segmentsWu3"),
    path('segments/marekova', app_views.segmentsMarekova, name="segmentsMarekova"),
    #path('segments/<str:dataset>', app_views.segments, name="segments"),

    path('_getSpeakers/', csrf_exempt(app_views.getSpeakers), name="getSpeakers"),
    path('_getSpeakerSegments/', csrf_exempt(app_views.getSpeakerSegments), name="getSpeakerSegments"),
    path('_makeSegmentsStats/', csrf_exempt(app_views.makeSegmentsStats), name="makeSegmentsStats"),
    path('_getTopSegments/', csrf_exempt(app_views.getTopSegments), name="getTopSegments"),
    path('_getWordSegments/', csrf_exempt(app_views.getWordSegments), name="getWordSegments")
]
